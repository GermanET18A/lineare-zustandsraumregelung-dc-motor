\babel@toc {ngerman}{}
\contentsline {chapter}{\numberline {1}Vorwort}{1}{chapter.3}%
\contentsline {chapter}{\numberline {2}Theoretischer Ansatz}{2}{chapter.4}%
\contentsline {chapter}{\numberline {3}Simulation in MATLAB Simulink}{8}{chapter.27}%
\contentsline {chapter}{\numberline {4}Anmerkungen}{11}{chapter.32}%
\contentsline {section}{\numberline {4.1}Mehrgrößensystem}{11}{section.33}%
\contentsline {section}{\numberline {4.2}LQ-Reglerentwurf}{11}{section.34}%
\contentsline {section}{\numberline {4.3}Störgrößenaufschaltung}{11}{section.35}%
\contentsline {chapter}{\nonumberline Abbildungsverzeichnis}{12}{chapter*.37}%
\contentsline {chapter}{\nonumberline Literaturverzeichnis}{13}{chapter*.38}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
