\chapter{Theoretischer Ansatz}
\label{cha:Ansatz}

In Abbildung \ref{fig:Gleichstrommotor} wird der Aufbau und das Funktionsprinzip eines Gleichstrommotors dargestellt.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\linewidth]{Bilder/Gleichstrommotor}
	\captionbelow[Aufbau und Funktionsweise eines Gleichstrommotors]{Aufbau und Funktionsweise eines Gleichstrommotors \citep{Wystup.2021b}}
	\label{fig:Gleichstrommotor}
\end{figure}
\noindent
Diese Funktionsweise des Gleichstrommotor lässt sich durch das Ersatzschaltbild aus Abbildung \ref{fig:Ersatzschaltbild} ausdrücken.
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{Bilder/Ersatzschaltbild}
	\captionbelow[Ersatzschaltbild eines Gleichstrommotors]{Ersatzschaltbild eines Gleichstrommotors \citep{Wystup.2021b}(bearbeitet)}
	\label{fig:Ersatzschaltbild}
\end{figure}
\noindent
Aus dem Ersatzschaltbild lässt sich mit Hilfe des 2. Kirchhoffschen Gesetzes die Maschengleichung
\begin{equation}
\label{eq:Maschengleichung}
	u=\underbrace{R\cdot i}_{\substack{u_R}}+\underbrace{L\cdot \frac{\delta i}{\delta t}}_{\substack{u_L}}+\underbrace{k_\phi\cdot\omega}_{\substack{u_{EMK}}}
\end{equation}
aufstellen \citep{Hagl.2015}.

\noindent
Des Weiteren lässt sich anhand des Ersatzschaltbildes das Momentengleichgewicht durch 
\begin{equation}
\label{eq:Momentengleichung}
	\underbrace{i\cdot k_\phi}_{\substack{M_{motor}}}-\underbrace{Lf\cdot\omega}_{\substack{M_{Last}}}=J\cdot\frac{\delta\omega}{\delta t}
\end{equation}
ausdrücken \citep{Hagl.2015}.
\\

\noindent
Durch mathematisches Umformen der beiden Gleichungen (\ref{eq:Maschengleichung}) und (\ref{eq:Momentengleichung}) lassen sich die folgenden beiden Differentialgleichungen, die die Gleichstrommaschine beschreiben, herleiten \citep{Ulrich.2020}:

\begin{equation}
	\frac{\delta i}{\delta t}=\frac{1}{L}\cdot (u - k_\phi\cdot \omega - R\cdot i)
\end{equation}

\begin{equation}
	\frac{\delta \omega}{\delta t}=\frac{1}{J}\cdot (k_\phi\cdot i - Lf\cdot \omega)
\end{equation}
\begin{center}
	\begin{singlespace}
			\begin{tabular}{rcl}
			$i$ & : & Motorstrom \\
			$\omega$ & : & Motordrehzahl \\
			$u$ & : & Spannung am Motor \\
			$L$ & : & Induktivität der Motorwicklung \\
			$R$ & : & ohmsch. Widerstand der Motorwicklung \\
			$J$ & : & Gesamtträgheitsmoment \\
			$k_\phi$ & : & Motorkonstante \\
			$Lf$ & : & Lastfaktor \\
		\end{tabular}
	\end{singlespace}
\end{center}

\noindent
Für die Zustandsraumbeschreibung werden diese in der Matrizenschreibweise, wie folgt, dargestellt:
\begin{equation}
\label{eq:Zustandsdifferentialgleichung}
	\underbrace{\frac{\delta}{\delta t}
		\left( \begin{array}{c}
			i \\
			\omega
		\end{array} \right)}_{\substack{\dot x}}
	=
	\underbrace{\left( \begin{array}{lr}
			-\frac{R}{L} & -\frac{k_\phi}{L}\\
			\frac{k_\phi}{J} & -\frac{Lf}{J}
		\end{array} \right)}_{\substack{A}}
	\cdot
	\underbrace{\left( \begin{array}{c}
			i \\
			\omega
		\end{array} \right)}_{\substack{x}}
	+
	\underbrace{\left( \begin{array}{c}
			\frac{1}{L} \\
			0
		\end{array} \right)}_{\substack{B}}	
	\cdot
	u
\end{equation}

\noindent
Aus der Gleichung \ref{eq:Zustandsdifferentialgleichung} ist zu entnehmen, dass es sich bei diesem Modell der Gleichstrommaschine um ein System mit einer Eingangsgröße $u$ und zwei Ausgangsgrößen $i$ und $\omega$, einem sogenannten \textbf{S}ingle \textbf{I}nput \textbf{M}ultiple \textbf{O}utput-System (SIMO-System), handelt\citep{Wystup.2021,Wikipedia.2021}.

Die Abbildung \ref{fig:Zustandsraumdarstellung} stell dar, dass es sich bei der Gleichung \ref{eq:Zustandsdifferentialgleichung} um die Zustandsdifferentialgleichung der Regelstrecke in der Zustandsraumdarstellung handelt. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\linewidth]{Bilder/Zustandsraumdarstellung}
	\captionbelow[Zustandsraummodell eines Eingrößensystems]{Zustandsraummodell eines Eingrößensystems \citep{Kuemmeke.2021}}
	\label{fig:Zustandsraumdarstellung}
\end{figure}

\noindent
Die Abbildung \ref{fig:Zustandsraumregelung} zeigt die allgemeine Darstellung einer Zustandsraumregelung. Daraus wird deutlich, dass für die Regelung der Regelstrecke ein Rückführglied \textbf{R} und Führungsfilter \textbf{V} hinzugefügt werden muss. Mit dem Rückführglied \textbf{R} wird der Zustand des Systems auf die Stellgröße u zurückgeführt. Mit dem Führungsfilter \textbf{V} wird die stationäre Führungsgenauigkeit bezweckt. Das zustandsgeregelte System wird durch folgende Gleichungen beschrieben \citep{Wystup.2021}:
\begin{equation}
\label{eq:Zustandsdifferentialgleichung des zustandsgeregelten Systems}
	\dot x= (\textbf{A}+\textbf{B}\textbf{R})\cdot \textbf{x}+\textbf{B}\textbf{V}\cdot w
\end{equation}
\begin{equation}
	\textbf{y}=\textbf{C}\cdot \textbf{x}
\end{equation}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.95\linewidth]{Bilder/Zustandsraumregelung}
	\captionbelow[Allgemeine Darstellung einer Zustandsraumregelung]{Allgemeine Darstellung einer Zustandsraumregelung \citep{Wystup.2021}}
	\label{fig:Zustandsraumregelung}
\end{figure}
\noindent
Die Kernaussage der Zustandsraumregelung lautet, dass sich das Schwingungsverhalten des Systems durch die Lage der Pole, den sogenannten Eigenwerten, bestimmen lässt. Diese Eigenwerte können wiederum, unter der Voraussetzung der Steuerbarkeit, willkürlich vorgegeben werden. Allgemein bestimmen sich die Eigenwerte des rückgeführten Systems als Nullstellen des charakteristische Polynoms nach \citep{Wystup.2021}:
\begin{equation}
\label{eq:Bestimmung der Eigenwerte}
	det(s\textbf{I}-\textbf{A}-\textbf{B}\textbf{R})=0
\end{equation}
Dabei wird die Hauptdiagonale der neuen Systemmatrix $(\textbf{A}+\textbf{B}\textbf{R})$ des zustandsgeregelten Systems um $-s$ erweitert und anschließend die Determinante der Systemmatrix gleich null gesetzt.

Mit Hilfe der Gleichung (\ref{eq:Bestimmung der Eigenwerte}) lässt sich somit die Rückführmatrix \textbf{R} und deren Komponenten bestimmen. Hierfür ist es zunächst erforderlich die Form der Matrix \textbf{R} festzulegen. Bei dem Eingangsvektor $\textbf{B}$ handelt es sich um einen Spaltenvektor. Um die Subtraktion mit der 2x2-Matrix $\textbf{A}$ durchführen zu können, muss die Rückführmatrix $\textbf{R}$ demnach ein Zeilenvektor sein. Unter Berücksichtigung dieses Aspekts und durch Ausformulieren ergibt sich folgender Ausdruck:
\begin{equation}
	det \left(
	\begin{pmatrix}
	s & 0\\
	0 & s
	\end{pmatrix}
	- 
	\begin{pmatrix}
	-\frac{R}{L} & -\frac{k_\phi}{L}\\
	\frac{k_\phi}{J} & -\frac{Lf}{J}
	\end{pmatrix}
	-
	\begin{pmatrix}
		\frac{1}{L} \\
		0
	\end{pmatrix}
	\cdot
	\begin{pmatrix}
		R_0 & R_1
	\end{pmatrix}
	\right) = 0
\end{equation}

\noindent
Unter Berücksichtigung der Rechenregeln ergibt sich der Ausdruck zu:
\begin{equation}
\label{eq:char_Polynom}
	s^{2}+(\frac{Lf}{J}+\frac{R}{L}-\frac{R_0}{L})\cdot s + \frac{1}{J\cdot L}\cdot (R\cdot Lf-R_0\cdot Lf-R_1\cdot k_\phi+k_\phi ^{2})=0
\end{equation}

\noindent
Ab diesem Punkt wird das weitere Vorgehen, anhand eines konkreten Beispiels erläutert. Da aufgrund des fehlenden Rechenprogramms, die Gleichungen in allgemeiner Form bei einer Rechnung mit der Hand zu unübersichtlich werden. Die allgemeine Gültigkeit der Vorgehensweise ist dabei nach wie vor gegeben.
Für die anstehenden Lösungsschritte werden folgende Werte angenommen:
$$
	R=1;\quad L=0.01;\quad k_\phi=0.5;\quad J=0.01;\quad Lf=0.1
$$

\noindent
Das charakteristische Polynom (\ref{eq:char_Polynom}) wird mit den eingesetzten Werten nach $s$ aufgelöst. Daraus ergeben sich die zwei Polstellen:
\begin{equation}
	s_{1,2}=-55+50\cdot R_0\pm \sqrt{2500\cdot R_0^2-4500\cdot R_0+5000\cdot R_1-475}
\end{equation}

\noindent
Anschließend wird sich die Tatsache zu Nutze gemacht, dass die Polstellen frei wählbar sind. Dadurch handelt es sich um ein lineares Gleichungssystem mit zwei Gleichungen und den beiden Unbekannten $R_0$ und $R_1$. Durch das Lösen des Gleichungssystems erhält man die beiden Komponenten $R_0$ und $R_1$ des Rückführvektors $\textbf{R}$:
\begin{align}
	R_0 &=\frac{s_1+s_2}{100}+1.1 \\
	R_1 &=\frac{1}{5000}(-s_1\cdot s_2-10\cdot(s_1+s_2)+2400)
\end{align}

\noindent
Für die Bestimmung des Führungsfilters \textbf{V} wird, anhand der Abbildung \ref{fig:Zustandsraumregelung}, die Matrix-Übertragungsfunktion von \underline{w} nach \underline{y} bestimmt \citep{Wystup.2021}:
\begin{equation}
\label{eq:Matrix Übertragungsfunktion}
\textbf{G}(s)=\textbf{C}(s\textit{I}-\textbf{A}-\textbf{B}\textbf{R})^{-1}\textbf{B}\textbf{V}
\end{equation}

\noindent
Um eine stationäre Regelabweichung zu vermeiden, muss die Grenzwertbetrachtung der Übertragungsfunktion für s = 0  den stationären Endwert
\begin{equation}
	\textbf{G}(0)=\textbf{I}
\end{equation}
ergeben.

Die Berücksichtigung dieser Bedingung liefert die Gleichung zur Bestimmung des Führungsfilters:
\begin{equation}
\label{eq:Führungsfilter}
	\textbf{V}=-(\textbf{C}(\textbf{A}+\textbf{B}\textbf{R})^{-1}\textbf{B})^{-1}
\end{equation}
Um die Gleichung lösen zu können, muss zunächst der Ausgangsvektor \textbf{C} bestimmt werden. Hierfür wird die Ausgangsgleichung
\begin{equation}
	y=\textbf{C}\cdot\textbf{x}
\end{equation}
\begin{center}
	\begin{tabular}{cl}
		mit & $\textbf{x}=\begin{pmatrix} i \\ \omega \end{pmatrix}$
	\end{tabular}
\end{center}
genutzt. Da in diesem Fall die Regelung der Kreisfrequenz $\omega$ relevant ist, also $ y = \omega$ und ein skalarer Wert sein muss, muss $$\textbf{C}=\begin{pmatrix}
	0 & 1
\end{pmatrix}$$ gewählt werden. Eine Kontrolle des gewählten Ausgangsvektors \textbf{C} bietet die Gleichung \ref{eq:Führungsfilter}. Denn eine Matrix lässt sich nur invertieren, wenn sie quadratisch ist. Bei der inneren Inversen handelt es sich um eine 2x2 Matrix und entspricht somit der Bedingung. Da es sich bei \textbf{B} um einen Spaltenvektor handelt, muss \textbf{C} ein Zeilenvektor sein, sodass durch die Matrixoperationen letztlich eine 1x1 Matrix entsteht. Nur so lässt sich auch die äußere Inverse bilden. Die hier getätigte Überlegung wird nachfolgend erläutert:
%\begin{align*}
%	\textbf{C}_{(1,2)}&\cdot (\textbf{A}+\textbf{BR})_{(2,2)}\cdot \textbf{B}_{(2,1)} \\
%	=
%	\begin{pmatrix}
%	1 & 1 
%	\end{pmatrix}
%	&\cdot
%	\begin{pmatrix}
%	1 & 1 \\ 1 & 1
%	\end{pmatrix}
%	\cdot
%	\begin{pmatrix}
%	1 \\ 1
%	\end{pmatrix} \\
%	=
%	\begin{pmatrix}
%		1 & 1 
%	\end{pmatrix}
%	&\cdot
%	\begin{pmatrix}
%		1 \\ 1
%	\end{pmatrix} \\
%	=
%	\begin{pmatrix}
%		1 
%	\end{pmatrix}
%\end{align*}
\begin{center}
	\begin{tabular}{p{1cm}p{1cm}p{0.5cm}p{2.5cm}p{0.5cm}p{1cm}}
		& $\textbf{C}_{(1,2)}$ & $\cdot$ & $(\textbf{A}+\textbf{BR})_{(2,2)}$ & $\cdot$ & $\textbf{B}_{(2,1)}$ \\[1ex]
		$=$ & $\begin{pmatrix}	1 & 1 \end{pmatrix}$ & $\cdot$ & $\begin{pmatrix} 1 & 1 \\ 1 & 1 \end{pmatrix}$ & $\cdot$ & $\begin{pmatrix} 1 \\ 1 \end{pmatrix}$ \\[3ex]
		$=$ &  &  & $\begin{pmatrix} 1 & 1 \end{pmatrix}$ & $\cdot$ & $\begin{pmatrix} 1 \\ 1 \end{pmatrix}$ \\[3ex]
		$=$ &  &  &  &  & $\begin{pmatrix}	 1  \end{pmatrix}$ \\
	\end{tabular}
\end{center}

\noindent
Nachdem die Form und die Komponenten des Ausgangsvektors \textbf{C} bestimmt sind, lässt sich \textbf{V} bestimmen. Durch Einsetzen der festgelegten Werte und Berücksichtigung der Rechenregeln bezüglich der Matrixoperationen und dem Bilden einer Inversen ergibt sich die Gleichung \ref{eq:Führungsfilter} zu: 
\begin{equation}
	\textbf{V}=\dfrac{10\cdot R_0+50\cdot R_1 -35}{-50}
\end{equation}

\noindent
Damit sind die beiden gesuchten Glieder bestimmt.

